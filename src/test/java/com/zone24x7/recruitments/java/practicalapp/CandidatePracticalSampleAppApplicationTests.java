package com.zone24x7.recruitments.java.practicalapp;

import com.zone24x7.recruitments.java.practicalapp.entity.Student;
import com.zone24x7.recruitments.java.practicalapp.repository.StudentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.assertj.core.api.Assertions.assertThat;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class CandidatePracticalSampleAppApplicationTests {

	@Mock
	private StudentRepository studentRepository;

	private Student student;

	@BeforeEach
	void setup() {
		student = Student.builder().firstName("John").lastName("Doe").build();
	}

	@Test
	public void should_not_be_null() {

		Student shouldReturnStudent = Student.builder().firstName("John").lastName("Doe").id(Long.valueOf("1")).build();
		Mockito.when(studentRepository.save(student)).thenReturn(shouldReturnStudent);
		Student persistedStudent = studentRepository.save(student);
		assertThat(persistedStudent).isNotNull();
	}

}
