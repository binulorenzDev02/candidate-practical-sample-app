package com.zone24x7.recruitments.java.practicalapp.repository;

import com.zone24x7.recruitments.java.practicalapp.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectRepository extends JpaRepository<Subject, Long> {

}
