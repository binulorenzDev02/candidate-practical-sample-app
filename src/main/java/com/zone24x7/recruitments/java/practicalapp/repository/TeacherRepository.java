package com.zone24x7.recruitments.java.practicalapp.repository;

import com.zone24x7.recruitments.java.practicalapp.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

}
