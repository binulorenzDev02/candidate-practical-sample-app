package com.zone24x7.recruitments.java.practicalapp.entity;


import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "student")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;

    private String lastName;

    private String address;

    private String contactNumber;

    private Date dateJoined;

    private String admissionNumber;

    private Boolean isWithSpecialNeeds;

    private String specialNeedRemark;

    private Boolean isSiblingsAvailable;

    private Integer numberOfCurrentSiblings;

    private String specialTalentRemark;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Subject> subjectList;


}
