package com.zone24x7.recruitments.java.practicalapp.repository;

import com.zone24x7.recruitments.java.practicalapp.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
