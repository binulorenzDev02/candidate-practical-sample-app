package com.zone24x7.recruitments.java.practicalapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CandidatePracticalSampleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(CandidatePracticalSampleAppApplication.class, args);
	}

}
